USB IrDA DYI adapter for Gossen Metrawatt Metrahit "Starline" series multimeters.

See http://lemmini.de/IrDA%20USB/IrDA%20USB.html

---------------------------------------------------------------
Copyright 2015 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.